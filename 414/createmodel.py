import numpy as np
from numpy.random import randint as rndi
from numpy.random import random as rnd

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense,Activation,Flatten,Dropout,Input,Concatenate,Dropout,Reshape,BatchNormalization,Conv2D
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential,Model
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
from tensorflow.keras.utils import plot_model

from functionals import *
from constants import *

from spec import spec



def createbothmodels(gs,out,n,shallvae=True,**kwargs):

  m=getm()

  s=state(gs=gs,param=None)
  g=grap(s)

  g,inn=prep(g,m)

  g=prevcut(g,ops=4)

  g=sortparam(g,m)
  g=pnorm(g)

  raw=g.X

  g=norm(g)

  ak=4
  if g.s.gs<ak:ak=g.s.gs

  g=gll(g,free=2,k=ak,m=m)

  for spe in spec:
    g=compress(g,c=spe,addparam=3,m=m)

    if g.s.gs>1:g=gnl(g)

  g,com,inn2=decompress(g,m,c=spec[::-1])



  g=remparam(g,nparam=4)

  g=sortparam(g,m)

  decom=g.X


  return handlereturn(inn,raw,com,inn2,decom,shallvae)












