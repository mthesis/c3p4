mkdir multi/2
python3 mmain.py 1 3 2
python3 eval.py 2
python3 bigeval.py 2
python3 recqual.py quiet 2
python3 testvloss.py 2

mkdir multi/3
python3 mmain.py 1 3 3
python3 eval.py 3
python3 bigeval.py 3
python3 recqual.py quiet 3
python3 testvloss.py 3
