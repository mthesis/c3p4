import numpy as np
import math
import sys

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense, Activation
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam,SGD
from tensorflow.linalg import trace







class ggraphrounder(Layer):
  def __init__(self,gs=30,numericalC=10000,**kwargs):
    
    self.gs=gs
    self.numericalC=numericalC

    super(ggraphrounder,self).__init__(input_shape=(gs,gs))

  def get_config(self):
    mi={"gs":self.gs,"numericalC":self.numericalC}
    th=super(ggraphrounder,self).get_config()
    th.update(mi)
    return th
  def from_config(config):
    return ggraphrounder(**config)
  
  def build(self, input_shape):


    self.built=True



  def call(self,x):
    x=x[0]


    #map anything above 0.5 to 1 and anything below to 0, who cares about 0(.5)
    #relu(1+C*(x-0.5))-relu(C*(x-0.5))

    C=self.numericalC

    rel=K.relu(1+C*(x-0.5))-K.relu(C*(x-0.5))
   
    #print("###############################") 
    #print("rel",rel.shape)
    #print("###############################") 
    #exit()

    return rel



    



    
  def compute_output_shape(self,input_shape):
    shape=list(input_shape[0])
    assert len(shape)==3
    assert shape[-1]==self.gs
    assert shape[-2]==self.gs

    return tuple([shape[0],self.gs,self.gs])














