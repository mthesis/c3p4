import numpy as np
from numpy.random import randint as rndi
from numpy.random import random as rnd

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense,Activation,Flatten,Dropout,Input,Concatenate,Dropout,Reshape,BatchNormalization
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential,Model
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
from tensorflow.keras.utils import plot_model

from gbuilder import *
from gtbuilder import *
from glbuilder import *
from gtlbuilder import *
from gcutter import *
from gpool import *
from gfeat import *
from gl import *
from gkeepbuilder import *
from glkeep import *
from gfeatkeep import *
from gkeepcutter import *
from gkeepmatcut import *
from gtopk import *
from gltk import *
from gltknd import *
from gpre1 import *
from gpre2 import *
from gpre3 import *
from gpre4 import *
from gpre5 import *
from glmlp import *
from gltrivmlp import *
from gadd1 import *
from gaddzeros import *
from gsym import *
from gbrokengrowth import *
from gpoolgrowth import *
from gremoveparam import *
from gcomdepool import *
from gcompool import *
from gvaluation import *
from ggoparam import *
from gfromparam import *
from gaddbias import *
from gssort import *
from winit import *
from glm import *
from glom import *#probably useless
from glim import *
from ggraphstract import *
from gmake1graph import *
from gcomdepoolplus import *


from createmodel import *




def createsupervised(gs,**kwargs):
  
  global m
  m=setting()
  m.usei=False
  m.decompress="classic"

  m.trivial_ladder_n=1
  m.trivial_decompress_activation="linear"
  m.trivial_decompress_init_kernel=t.keras.initializers.TruncatedNormal()
  m.trivial_decompress_init_bias=t.keras.initializers.TruncatedNormal()

  m.sortindex=-1
  m.prenorm=False

  m.graph_init_self=t.keras.initializers.TruncatedNormal()
  m.graph_init_neig=t.keras.initializers.TruncatedNormal()
  m.agraph_init_self=m.graph_init_self
  m.agraph_init_neig=m.graph_init_neig

  m.edges=3#particle net like
  m.edgeactivation="relu"
  m.edgeactivationfinal=m.edgeactivation
  m.edgeusebias=False
  m.edgeconcat=False

  m.gq_activation="relu"
  m.gq_init_kernel=tf.keras.initializers.TruncatedNormal()
  m.gq_init_bias=tf.keras.initializers.Zeros()
  m.gq_usebias=False
  m.gq_batchnorm=False

  m.shallcomplex=not True
  m.complexsteps=3

  m.gqa_activation=m.gq_activation
  m.gqa_init_kernel=m.gq_init_kernel
  m.gqa_init_bias=m.gq_init_bias
  m.gqa_usebias=m.gq_usebias
  m.gqa_batchnorm=m.gq_batchnorm

  m.shallacomplex=m.shallcomplex
  m.complexasteps=m.complexsteps

  m.shallredense=False
  m.redenseladder=[8,6]
  m.redenseactivation="relu"
  m.redenseinit=keras.initializers.Identity()

  m.compression_init=keras.initializers.Identity()

  m.mdense_activation="relu"
  m.mdense_init_kernel=t.keras.initializers.TruncatedNormal()
  m.mdense_init_bias=t.keras.initializers.TruncatedNormal()
  m.mdense_usebias=True
  m.mdense_batchnorm=False



  
  s=state(gs=gs,param=None)
  g=grap(s)



  g,inputs=prep(g,m=m)
  g=norm(g)

  #g=gll(g,m=m)
  #g=gnl(g,m=m)
  #g=gll(g,m=m)
  #g=gll(g,m=m)
  #g=gll(g,m=m)

  #g=abstr(g,c=gs,m=m)
  
  g=goparam(g,chanceS=True)
  
  g=multidense(g,m,[8,2])

  
  outputs=g.X
  outputs=Activation("softmax")(outputs)


  return Model(inputs=inputs,outputs=outputs)









