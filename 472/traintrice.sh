rm -r multi
mkdir multi
mkdir multi/1
python3 mmain.py 1 2 1
python3 eval.py 1
python3 bigeval.py 1
python3 recqual.py quiet 1
python3 testvloss.py 1

mkdir multi/2
python3 mmain.py 1 2 2
python3 eval.py 2
python3 bigeval.py 2
python3 recqual.py quiet 2
python3 testvloss.py 2

mkdir multi/3
python3 mmain.py 1 2 3
python3 eval.py 3
python3 bigeval.py 3
python3 recqual.py quiet 3
python3 testvloss.py 3
