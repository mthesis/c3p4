import numpy as np
import math

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense, Activation
import tensorflow.keras as keras# as k
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam,SGD
from tensorflow.linalg import trace





class dnarrow(Layer):
  def __init__(self,gs=20,param=40,**kwargs):
    self.gs=gs
    self.param=param
    super(dnarrow,self).__init__(**kwargs)

  def build(self, input_shape):

    self.built=True

  def call(self,x):
    a=x#[0]
    print("calling",a.shape)
    #exit()  

 
    return a[:,:,0,:]

 
  def compute_output_shape(self,input_shape):
    a=input_shape#[0]
    print("inputting",a)
    #exit()
    assert len(a)==4
    assert a[1]==self.gs
    assert a[2]==self.param
    return tuple([input_shape[0],self.gs,self.param])#here no new parameters   

  def get_config(self):
    mi={"gs":self.gs,"param":self.param}
    th=super(dnarrow,self).get_config()
    th.update(mi)
    return th
  def from_config(config):
    return dnarrow(**config)































