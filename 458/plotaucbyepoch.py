import numpy as np
import matplotlib.pyplot as plt
import sys

q="vl"

if len(sys.argv)>1:
  q=sys.argv[1]

base=""
if len(sys.argv)>2:
  base="multi/"+sys.argv[2]+"/"

f=np.load(base+"abe2.npz")


l=f[q]
a=f["a"]



plt.plot(l,a,"o")

c=np.corrcoef([l,a])[0,1]
lc=np.corrcoef([np.log(l),np.log(a)])[0,1]

plt.title(str(round(c,4))+"("+str(round(lc,4))+")")



plt.xlabel(q)
plt.ylabel("auc")

plt.savefig(base+"lossbyauc.png",format="png")
plt.savefig(base+"lossbyauc.pdf",format="pdf")

plt.show()














