import numpy as np
import matplotlib.pyplot as plt
from os.path import isfile
import sys


mode=0
if len(sys.argv)>1:mode=int(sys.argv[1])

f=np.load("bigevalb.npz")



p=f["p"]
c=f["c"]

l=np.sqrt(np.mean((p-c)**2,axis=(-1,-2)))

c=np.sum(l>100)

print(c)

if mode%2==0:plt.hist(l,bins=200,log=True,density=True,label="bigeval")


if isfile("nd.npz") and mode>0:
  f2=np.load("nd.npz")
  d2=f2["d"]
  if mode==2:
    d2=np.array([ac for ac in d2 if ac<1])
  plt.hist(d2,bins=200,log=True,density=True,label="nd")
  plt.legend()


plt.show()


