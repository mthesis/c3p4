#generates a lot of pictures, displaying the values of one parameter each, once for qcd and once for tops
#here alterations to be able to also display other(y==2) data

import numpy as np
import matplotlib.pyplot as plt

from adv2load import *

f=np.load("bcode.npz")
f2=np.load("bcodesv.npz")

y=np.concatenate((f["y"],f2["y"]))
p=np.concatenate((f["p"],f2["p"]))

alpha=0.5
bins=200


offset=980

my=int(np.max(y))

for i in range(offset,len(p[0])):
#q=np.sum(p,axis=-1)
  q=p[:,i]

  plt.title(str(np.mean(q))+","+str(np.std(q))+","+str(np.min(q))+","+str(np.max(q)))

  min=np.min(q)
  max=np.max(q)

  max=0.0000001
  min=-max

  for k in range(my+1):
    aq=q[y==k]
    aq=aq[np.logical_and(aq>min,aq<max)]
    print(len(aq),np.mean(aq),"+-",np.std(aq))
    plt.hist(aq,alpha=alpha,bins=bins,label=str(k),range=(min,max))

  plt.legend()
  plt.show()
  print("did",i,len(p[0]))
  plt.close()
  
  exit()



