import numpy as np
import matplotlib.pyplot as plt
import sys
from cauc import caucd



quiet=False
if len(sys.argv)>1:
  quiet=sys.argv[1]=="quiet"





f=np.load("superevalb.npz")

x=f["x"]
y=f["y"]
p=f["p"]


p=p[:,-1]

#print(y)

#exit()


q=caucd(d=p,y=y)

auc=q["auc"]
i30=q["i30"]
e30=q["e30"]
tpr=q["tpr"]
fpr=q["fpr"]
d0=q["d0"]
d1=q["d1"]

print("auc:",auc,"   at",tpr[i30],":",e30,"=1/",1/e30)





np.savez_compressed("superroc",**q)

#plt.hist(d,bins=200)
plt.hist(d0,bins=100,alpha=0.5,label="0")
plt.hist(d1,bins=100,alpha=0.5,label="1")

plt.legend()

plt.xlabel("mse")
plt.ylabel("#")

plt.savefig("imgs/superrecqual.png",format="png")
plt.savefig("imgs/superrecqual.pdf",format="pdf")

if not quiet and False:plt.show()

plt.close()



plt.plot(fpr,tpr,label="auc="+str(round(auc,4)),color="darkblue")
plt.plot(tpr,fpr,label="auc="+str(round(1-auc,4)),color="dodgerblue")

plt.plot(np.arange(0,1,0.01),np.arange(0,1,0.01),color="grey",alpha=0.3)



plt.legend()
plt.xlabel("false positive rate (is true, but classified as false)")
plt.ylabel("true positive rate (is true, and classified as true)")

plt.savefig("imgs/superroc.png",format="png")
plt.savefig("imgs/superroc.pdf",format="pdf")
if not quiet:plt.show()


















